package ro.sci.siit.aut.learning.tests.examples.run;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.*;

public class DBExamples {
    private Connection conn = null;
    private ResultSet resultSet = null;
    private Statement statement = null;

    @Test
    public void testTheDB() throws SQLException {
        ResultSet resultSet = executeStatement("SELECT * FROM students");
        if (resultSet != null) {
            while(resultSet.next())
            {
                System.out.println(resultSet.getString("id") + resultSet.getString("first_name") +
                        resultSet.getString("last_name") + resultSet.getString("email_address") +
                resultSet.getString("team_id") + resultSet.getString("county_id") +
                resultSet.getString("company"));

            }
            for (int i = 1; i<=4; i++){
                System.out.println("Team " + i +" has the following members");

                    while(resultSet.next()){
                        int teamNumber = resultSet.getInt("team_id");
                        System.out.println(resultSet.getString("team_id"));
                        if (teamNumber == i){
                            System.out.println(resultSet.getString("first_name"));
                            System.out.println(resultSet.getString("team_id"));
                        }
                    }
            }
        }
    }

    @AfterMethod
    private void closeConnection() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @BeforeMethod
    private void connect() {
        try {

            String db_host = "jdbc:mysql://192.168.2.179:3306/automation";
            String db_user = "aut";
            String db_pass = "aut";
            conn = DriverManager.getConnection(db_host, db_user, db_pass);
            statement = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private ResultSet executeStatement(String q) {
        try {
            resultSet = statement.executeQuery(q);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultSet;
    }
}
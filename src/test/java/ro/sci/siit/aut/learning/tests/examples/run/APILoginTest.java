package ro.sci.siit.aut.learning.tests.examples.run;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Larry on 08.08.2016.
 */
public class APILoginTest {
    @Test
    public void testingLoginAPI() throws IOException {

        Response r = Request.Post("http://shop-tausandbox.rhcloud.com/index.php?route=account/login")
                .bodyForm(
                        Form.form()
                        .add("email","autotestlarry2@yopmail.com")
                        .add("password", "password").build())
                .execute();

        HttpResponse httpResponse = r.returnResponse();
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        String reasonPhrase = httpResponse.getStatusLine().getReasonPhrase();
        System.out.println("Response was: " +  statusCode + " - " + reasonPhrase);

        Header[] headersList = httpResponse.getAllHeaders();
        for (int i = 0; i < headersList.length; i++){
            System.out.println(headersList[i].toString());
        }
    }
}

package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.model.RegistrationModel;

/**
 * Created by Larry on 13.06.2016.
 */
public class OldRegistrationTest {
    WebDriver driver;

    @BeforeMethod
    public void startFirefox() {
        driver = new FirefoxDriver();
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/registration.html");
    }

    @AfterMethod
    public void stopFirefox() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @DataProvider(name = "RegistrationDataProvider")
    public Object[][] dataProviderForRegistrarionTest() {
        Object[][] dpObject = new Object[3][2];

        RegistrationModel model = new RegistrationModel("", "", "", "", "", "", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please fill out this field.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Please fill out this field.",
                "Please fill out this field.");
        dpObject[0][0] = "failed";
        dpObject[0][1] = model;

        model = new RegistrationModel("", "", "notAnEmail", "", "short", "short", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please enter an email address.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Not long enough",
                "");

        dpObject[1][0] = "failed";
        dpObject[1][1] = model;

        model = new RegistrationModel("", "", "notAnEmail", "", "password", "short", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please enter an email address.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Minimum of 8 characters",
                "The passwords don't match");

        dpObject[2][0] = "failed";
        dpObject[2][1] = model;

        return dpObject;
    }

    @Test(dataProvider = "RegistrationDataProvider")
    public void registrationTest(String expected, RegistrationModel model) {


        WebElement firstNameField = driver.findElement(By.id("inputFirstName"));
        firstNameField.clear();
        firstNameField.sendKeys(model.getFirstName());

        WebElement lastNameField = driver.findElement(By.id("inputLastName"));
        lastNameField.clear();
        lastNameField.sendKeys(model.getLastName());

        WebElement emailField = driver.findElement(By.id("inputEmail"));
        emailField.clear();
        emailField.sendKeys(model.getEmail());

        WebElement usernameField = driver.findElement(By.id("inputUsername"));
        usernameField.clear();
        usernameField.sendKeys(model.getUsername());

        WebElement passwordField = driver.findElement(By.id("inputPassword"));
        passwordField.clear();
        passwordField.sendKeys(model.getPassword());

        WebElement confirmPasswordField = driver.findElement(By.id("inputPassword2"));
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(model.getConfirmPassword());

        WebElement bDayField = driver.findElement(By.id("bday"));
        Select selectBday = new Select(bDayField);
        selectBday.selectByValue(model.getDayOfBirth());

        WebElement bMonthField = driver.findElement(By.id("bmonth"));
        Select selectBmonth = new Select(bMonthField);
        selectBmonth.selectByValue(model.getMonthOfBirth());

        WebElement bYearField = driver.findElement(By.id("byear"));
        Select selectByear = new Select(bYearField);
        selectByear.selectByValue(model.getYearOfBirth());

        if (expected == "failed") {
            WebElement firstNameError = driver.findElement(By.xpath("//input[@id='inputFirstName']/following-sibling::div"));
            String firstNameErrorMessage = firstNameError.getText();
            Assert.assertEquals(firstNameErrorMessage, model.getFirstNameError());

            WebElement lastNameError = driver.findElement(By.xpath("//input[@id='inputLastName']/following-sibling::div"));
            String lastNameErrorMessage = lastNameError.getText();
            Assert.assertEquals(lastNameErrorMessage, model.getLastNameError());

            WebElement emailError = driver.findElement(By.xpath("//input[@id='inputEmail']/following-sibling::div"));
            String emailErrorMessage = emailError.getText();
            Assert.assertEquals(emailErrorMessage, model.getEmailError());

            WebElement usernameError = driver.findElement(By.xpath("//input[@id='inputUsername']/following-sibling::div"));
            String usernameErrorMessage = usernameError.getText();
            Assert.assertEquals(usernameErrorMessage, model.getUsernameError());

            WebElement passwordError = driver.findElement(By.xpath("//input[@id='inputPassword']/following-sibling::div"));
            String passwordErrorMessage = passwordError.getText();
            Assert.assertEquals(passwordErrorMessage, model.getPasswordError());

            WebElement confirmPasswordError = driver.findElement(By.xpath("//input[@id='inputPassword2']/following-sibling::div"));
            String confirmPasswordErrorMessage = confirmPasswordError.getText();
            Assert.assertEquals(confirmPasswordErrorMessage, model.getConfirmPasswordError());
        }
    }
}
//RegistrationModel model = new RegistrationModel("firstName", "lastName", "email@email", "username", "password", "password", "29", "12", "1986",
//      "1", "2", "3", "4", "5", "6");
//dpObject[0][0] = "failed";
//        dpObject[0][1] = model;

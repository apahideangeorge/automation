package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Test;

/**
 * Created by Larry on 06.06.2016.
 */
public class DriverExamples {
    @Test
    public void chromeDriverTest() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://google.com");
        driver.quit();
    }

    @Test
    public void ieDriverTest() {
        System.setProperty("webdriver.ie.driver", "src/test/resources/IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver();
        driver.get("http://google.com");
        driver.close();
    }

    @Test
    public void htmlUnitDriverTest() {
        WebDriver htmlUnitDriver = new HtmlUnitDriver();
        htmlUnitDriver.get("http://www.google.com");
        htmlUnitDriver.quit();
    }

}

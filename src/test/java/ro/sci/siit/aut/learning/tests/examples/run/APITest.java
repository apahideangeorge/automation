package ro.sci.siit.aut.learning.tests.examples.run;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.xmlbeans.impl.common.IOUtil;
import org.testng.annotations.Test;


import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Larry on 08.08.2016.
 */
public class APITest {
    @Test
    public void testingAPI() throws IOException{
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://shop-tausandbox.rhcloud.com");
        CloseableHttpResponse httpResponse = httpclient.execute(httpGet);

        int statusCode = httpResponse.getStatusLine().getStatusCode();
        String reasonPhrase = httpResponse.getStatusLine().getReasonPhrase();
        System.out.println("Response was: " +  statusCode + " - " + reasonPhrase);

        InputStream bodyAsInputStream = httpResponse.getEntity().getContent();
        String responseBody = IOUtils.toString(bodyAsInputStream);
        System.out.println(responseBody);
    }
}

package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.Browsers;
import ro.sci.siit.aut.learning.WebBrowser;

public class OldTestClassA {
    private WebDriver driver;

    @AfterMethod(alwaysRun = true)
    public void closeBrowser() {
        if (driver != null)
            driver.quit();
    }

    @Test(groups = "G1")
    public void test1() {
        System.out.println("I am test A.1");
        System.out.println("I should pass");
    }

    @Test(groups = "G2")
    public void test2() {
        System.out.println("I am test A.2");
        System.out.println("I should pass");
    }

    @Test(groups = {"G1", "G2"})
    public void test3() {
        System.out.println("I am test A.3");
        System.out.println("I should pass");
    }

    @Parameters({"p1", "p2"})
    @Test(groups = {"G3"})
    public void test4(String p1, String p2) {
        System.out.println("I am test A.4");
        System.out.println("I should pass");
        System.out.println("I also have parameters: " + p1 + " and " + p2);
    }

    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void test5(String browser) {
        System.out.println("I am test A.5");
        driver = WebBrowser.getDriver(Browsers.valueOf(browser));
        driver.get("http://google.com");
    }

    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void test6(String browser) {
        System.out.println("I am test A.6");
        driver = WebBrowser.getDriver(Browsers.valueOf(browser));
        driver.get("http://google.com");
    }

    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void test7(String browser) {
        System.out.println("I am test A.7");
        driver = WebBrowser.getDriver(Browsers.valueOf(browser));
        driver.get("http://google.com");
    }

    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void test8(String browser) {
        System.out.println("I am test A.8");
        driver = WebBrowser.getDriver(Browsers.valueOf(browser));
        driver.get("http://google.com");
    }

    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void test9(String browser) {
        System.out.println("I am test A.9");
        driver = WebBrowser.getDriver(Browsers.valueOf(browser));
        driver.get("http://google.com");
    }
}
package ro.sci.siit.aut.learning.model;

/**
 * Created by Larry on 10.08.2016.
 */
public class PostModel {
    private String id;
    private String title;
    private String author;


    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}
    public String getBody() {return author;}
    public void setBody(String body) {this.author = body;}
    public String getUserID() {return id;}
    public void setUserID(String userID) {this.id = userID;}
}

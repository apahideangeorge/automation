//package ro.sci.siit.aut.learning.tests.examples.run;
//
//import org.testng.Assert;
//import org.testng.SkipException;
//import org.testng.annotations.Test;
//
//public class TestClassA extends RunBaseTest {
//
//    @Test(groups = "G1", description = "Description of testA1")
//    public void testA1() {
//        testLogger("I should pass");
//        Assert.assertTrue(true, "Checking that true is true");
//    }
//
//    @Test(groups = "G2", description = "Description of testA2")
//    public void testA2() {
//        testLogger("I should fail");
//        Assert.assertTrue(false, "Checking that false is true");
//    }
//
//    @Test(groups = {"G1", "G2"}, description = "Description of testA3")
//    public void testA3() {
//        testLogger("I should be skipped");
//        throw new SkipException("I am skipping this test. Because I can!");
//    }
//}

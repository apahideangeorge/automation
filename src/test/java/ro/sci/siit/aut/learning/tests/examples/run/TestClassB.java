//package ro.sci.siit.aut.learning.tests.examples.run;
//
//import org.testng.Assert;
//import org.testng.SkipException;
//import org.testng.annotations.Test;
//
//public class TestClassB extends RunBaseTest {
//
//    @Test(groups = "G1")
//    public void testB1() {
//        testLogger("I should pass");
//        Assert.assertTrue(true, "Checking that true is true");
//    }
//
//    @Test(groups = "G2")
//    public void testB2() {
//        testLogger("I should fail");
//        Assert.assertFalse(true, "Checking that true is false");
//    }
//
//    @Test(groups = {"G1", "G2"})
//    public void testB3() {
//        testLogger("I should be skipped");
//        throw new SkipException("I am skipping this test. Because I can!");
//    }
//}

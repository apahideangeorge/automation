package ro.sci.siit.aut.learning.tests.examples;

import org.testng.annotations.Test;

public class OldTestClassB {

    @Test(groups = "G1")
    public void test1() {
        System.out.println("I am test B.1");
        System.out.println("I should pass");
    }

    @Test(groups = "G2")
    public void test2() {
        System.out.println("I am test B.2");
        System.out.println("I should pass");
    }

    @Test(groups = {"G1", "G2"})
    public void test3() {
        System.out.println("I am test B.3");
        System.out.println("I should pass");
    }

}
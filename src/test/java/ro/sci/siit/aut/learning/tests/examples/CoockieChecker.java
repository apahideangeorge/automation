package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.tests.BaseTest;

/**
 * Created by Larry on 03.07.2016.
 */
public class CoockieChecker extends BaseTest {
    @BeforeMethod
    public void startBrowser() {
        driver.get("http://google.com");
    }

    @Test
    public void noCookie() {
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/cookie.html");
        WebElement cookieButton = driver.findElement(By.id("set-cookie"));
        cookieButton.click();

        WebElement deleteCookieButton = driver.findElement(By.id("delete-cookie"));
        deleteCookieButton.click();

        WebElement cookieText = driver.findElement(By.id("cookie-value"));
        Cookie c = driver.manage().getCookieNamed("gibberish");
        Assert.assertNull(c);
        Assert.assertEquals(cookieText.getText(), "");
    }
}
package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/**
 * Created by Larry on 19.06.2016.
 */
public class PushTheLazyButton10Times {
    @Test
    public void chromeDriverTest() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");

        for (int i = 1; i <= 10; i++)
        {
            WebDriver driver = new ChromeDriver();
            driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/lazy.html");


            long start = System.currentTimeMillis();
            WebDriverWait wait = new WebDriverWait(driver, 5);
            WebElement myDynamicElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("lazy-button")));
            WebElement lazyButton = driver.findElement(By.id("lazy-button"));
            long finish = System.currentTimeMillis();
            long totalTime = finish - start;
            System.out.println("Total Time for lazy button - "+totalTime/1000);
            lazyButton.click();
            driver.quit();

        }

    }
}

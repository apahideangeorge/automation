package ro.sci.siit.aut.learning.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ro.sci.siit.aut.learning.Browsers;
import ro.sci.siit.aut.learning.WebBrowser;

/**
 * Created by Larry on 27.06.2016.
 */
public class BaseTest {
    public WebDriver driver;

    @BeforeMethod
    public void setup() {
        driver = WebBrowser.getDriver(Browsers.CHROME);
    }

    @AfterMethod
    public void stopBrowser() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }
}

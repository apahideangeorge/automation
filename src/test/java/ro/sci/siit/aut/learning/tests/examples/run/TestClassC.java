//package ro.sci.siit.aut.learning.tests.examples.run;
//
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.Parameters;
//import org.testng.annotations.Test;
//import ro.sci.siit.aut.learning.Browsers;
//import ro.sci.siit.aut.learning.WebBrowser;
//
//import java.util.Random;
//
//public class TestClassC extends RunBaseTest {
//
//    @Parameters("listOfBrowsers")
//    @Test(description = "Test that uses a random browser")
//    public void randomBrowser(String listOfBrowsers) {
//        testLogger("I have a parameter called 'browsers'. The parameter value is: " + listOfBrowsers);
//        Browsers b = Browsers.valueOf(getRandomBrowser(listOfBrowsers).toUpperCase());
//        WebDriver driver = WebBrowser.getDriver(b);
//        driver.manage().window().maximize();
//        driver.get("http://scoalainformala.ro");
//        testLogger(driver.getTitle());
//        driver.quit();
//    }
//
//    @Parameters({"p1", "p2"})
//    @Test(groups = {"G3"})
//    public void testC1(String p1, String p2) {
//        testLogger("I should pass");
//        testLogger("I also have parameters: " + p1 + " and " + p2);
//    }
//
//    @Parameters({"browser"})
//    @Test(groups = {"parallel"})
//    public void testC2(String browser) {
//        testLogger("I have a parameter called 'browser'. The parameter value is: " + browser);
////		driver = WebBrowser.getDriver(Browsers.valueOf(browser.toUpperCase()));
////		driver.get("http://google.com");
//    }
//
//    private String getRandomBrowser(String listOfBrowsers) {
//        String[] browsers = listOfBrowsers.split(",");
//        int random = new Random().nextInt(browsers.length);
//        return browsers[random];
//    }
//}

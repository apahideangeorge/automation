package ro.sci.siit.aut.learning.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.model.LoginModel;
import ro.sci.siit.aut.learning.pages.LoginPage;

/**
 * Created by Larry on 15.06.2016.
 */
public class LoginTest {
    WebDriver driver;

    @BeforeMethod
    public void startFirefox() {
        driver = new FirefoxDriver();
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/login.html");
    }

    @AfterMethod
    public void stopFirefox() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }



    @DataProvider(name = "LoginDataProvider")
    public Object[][] dataProviderForSearchTest() {
        Object[][] dpObject = new Object[5][2];

        LoginModel model = new LoginModel("Some User", "somePassword", "", "", "Incorrect username or password");
        dpObject[0][0] = "failed";
        dpObject[0][1] = model;

        model = new LoginModel("", "", "Please enter your username", "Please enter your password",
                "Incorrect username or password");
        dpObject[1][0] = "failed";
        dpObject[1][1] = model;

        model = new LoginModel("Some User", "", "", "Please enter your password", "Incorrect username or password");
        dpObject[2][0] = "failed";
        dpObject[2][1] = model;

        model = new LoginModel("", "somePassword", "Please enter your username", "", "Incorrect username or password");
        dpObject[3][0] = "failed";
        dpObject[3][1] = model;

        model = new LoginModel("user", "password", "", "", "");
        dpObject[4][0] = "success";
        dpObject[4][1] = model;

        return dpObject;
    }

    @DataProvider(name = "LoginDataProviderSuccessfull")
    public Object[][] dataProviderForSuccessfullSearchTest() {
        Object[][] dpObject = new Object[1][2];

        LoginModel model = new LoginModel("user", "password", "", "", "");
        dpObject[0][0] = "success";
        dpObject[0][1] = model;
        return dpObject;
    }

    @Test (dataProvider = "LoginDataProvider")
    public void loginTest(String expected, LoginModel model){
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.login(model.getUsername(),model.getPassword());

        if (expected == "failed") {
            Assert.assertEquals(loginPage.userError(), model.getUsernameError());
            Assert.assertEquals(loginPage.passwordError(), model.getPasswordError());
            Assert.assertEquals(loginPage.generalError(), model.getGeneralError());
        } else {
            WebElement logoutButton = driver.findElement(By.id("logout-submit"));
            String logoutButtonText = logoutButton.getText();
            Assert.assertEquals(logoutButtonText, "Logout");
        }
    }
}
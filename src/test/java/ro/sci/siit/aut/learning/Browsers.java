package ro.sci.siit.aut.learning;

/**
 * Created by Larry on 27.06.2016.
 */
public enum Browsers {
    CHROME,
    FIREFOX,
    IE
}

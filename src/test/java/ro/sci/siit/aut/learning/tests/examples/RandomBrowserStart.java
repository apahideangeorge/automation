package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.Browsers;
import ro.sci.siit.aut.learning.WebBrowser;

import java.util.Random;

/**
 * Created by Larry on 06.07.2016.
 */
public class RandomBrowserStart {
    private WebDriver driver;



    @Parameters({"browser"})
    @Test(groups = {"parallel"})
    public void browserStart (String browser) {
        String [] b = browser.split(":");
        int index = new Random().nextInt(b.length);
        String randomBrowser = b[index];

        driver = WebBrowser.getDriver(Browsers.valueOf(randomBrowser));
        driver.get("http://google.com");
    }
}

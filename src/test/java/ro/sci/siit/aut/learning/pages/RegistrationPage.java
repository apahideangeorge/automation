package ro.sci.siit.aut.learning.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Larry on 25.06.2016.
 */
public class RegistrationPage {

    @FindBy (how = How.ID, using = "inputFirstName")
    private WebElement firstNameField;

    @FindBy (how = How.XPATH, using = "//input[@id='inputFirstName']/following-sibling::div")
    private WebElement firstNameErrorElement;

    @FindBy (how = How.ID, using = "inputLastName")
    private WebElement lastNameField;

    @FindBy (how = How.XPATH, using = "//input[@id='inputLastName']/following-sibling::div")
    private WebElement lastNameErrorElement;

    @FindBy (how = How.ID, using = "inputEmail")
    private WebElement emailField;

    @FindBy (how = How.XPATH, using = "//input[@id='inputEmail']/following-sibling::div")
    private WebElement emailErrorElement;

    @FindBy (how = How.ID, using = "inputUsername")
    private WebElement usernameField;

    @FindBy (how = How.XPATH, using = "//input[@id='inputUsername']/following-sibling::div")
    private WebElement usernameErrorElement;

    @FindBy (how = How.ID, using = "inputPassword")
    private WebElement passwordField;

    @FindBy (how = How.XPATH, using = "//input[@id='inputPassword']/following-sibling::div")
    private WebElement passwordErrorElement;

    @FindBy (how = How.ID, using = "inputPassword2")
    private WebElement passwordField2;

    @FindBy (how = How.XPATH, using = "//input[@id='inputPassword2']/following-sibling::div")
    private WebElement passwordError2Element;

    @FindBy (how = How.ID, using = "bday")
    private WebElement dayOfBirthField;

    @FindBy (how = How.ID, using = "bmonth")
    private WebElement monthOfBirthField;

    @FindBy (how = How.ID, using = "byear")
    private WebElement yearOfBirthField;

    public String firstNameError() {return firstNameErrorElement.getText();}
    public String lastNameError() {return lastNameErrorElement.getText();}
    public String emailError() {return emailErrorElement.getText();}
    public String usernameError() {return usernameErrorElement.getText();}
    public String passwordError() {return passwordErrorElement.getText();}
    public String passwordError2() {return passwordError2Element.getText();}

    public void register (String firstName, String lastName, String email, String username,
                          String password, String password2, String dayOfBirth, String monthOfBirth, String yearOfBirth){

        firstNameField.clear();
        firstNameField.sendKeys(firstName);

        lastNameField.clear();
        lastNameField.sendKeys(lastName);

        emailField.clear();
        emailField.sendKeys(email);

        usernameField.clear();
        usernameField.sendKeys(username);

        passwordField.clear();
        passwordField.sendKeys(password);

        passwordField2.clear();
        passwordField2.sendKeys(password2);

        dayOfBirthField.sendKeys(dayOfBirth);

        monthOfBirthField.sendKeys(monthOfBirth);

        yearOfBirthField.sendKeys(yearOfBirth);
    }
}

package ro.sci.siit.aut.learning;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Created by Larry on 27.06.2016.
 */
public class WebBrowser {

    public static WebDriver getDriver(Browsers browser){
        WebDriver driver;
        switch (browser){

            case CHROME:
                System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
                //driver = new ChromeDriver(); start chrome with default settings
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("test-type");
                chromeOptions.addArguments("start-maximized");
                driver = new ChromeDriver(chromeOptions);
                break;

            case FIREFOX:
                driver = new FirefoxDriver();
                //System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver.exe");
                //driver = new MarionetteDriver();

                //Firefox custom profile
                //FirefoxProfile profile = new FirefoxProfile();
                //profile.addExtension(extension_file_here);
                //DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                //desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);
                //desiredCapabilities.setCapability(FirefoxDriver.BINARY, "path_to_binary");
                break;

            case IE:
                System.setProperty("webdriver.ie.driver", "src/test/resources/IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;

            default:
                throw new RuntimeException("Unknown browser:+ " + browser.toString());

        }
        return driver;
    }
}

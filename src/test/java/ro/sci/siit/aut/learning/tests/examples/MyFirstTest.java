package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.List;

/**
 * Created by Larry on 30.05.2016.
 */



public class MyFirstTest {
    WebDriver driver; //a "driver" object is created

    @BeforeMethod
    public void startFirefox(){
        driver = new FirefoxDriver();
        driver.get("http://google.com");
    }

    @AfterMethod
    public void stopFirefox() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }
    @Test // TestNG annotation
    public void myFirstTest(){

        // initialize a new driver object
        //WebDriver driver = new FirefoxDriver();

        // call method get() from driver object
        //driver.get("http://www.google.com");

        //create an object of type WebElement

        WebElement searchBox = driver.findElement(By.id("lst-ib"));
        searchBox.sendKeys("koala");

        WebElement searchButton = driver.findElement(By.name("btnG"));
        searchButton.click();

        WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));
        System.out.println(driver.findElement(By.id("resultStats")).getText());

        List<WebElement> resultTitleElements = driver.findElements(By.tagName("h3"));
        System.out.println(resultTitleElements.size());
        for (WebElement we : resultTitleElements)
        {
            System.out.println(we.getText());
            WebElement resultUrl = we.findElement(By.xpath("//a"));
            String url = resultUrl.getAttribute("href");
            System.out.println(url);
        }







        //WebElement numberOfResults = driver.findElement(By.id("resultStats"));
        //numberOfResults.getText();
        //System.out.println(numberOfResults.getText());

        //WebElement resultList = driver.findElements(By.class("r"))
        //resultList.getText();
    }
}



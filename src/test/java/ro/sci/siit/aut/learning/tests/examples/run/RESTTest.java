package ro.sci.siit.aut.learning.tests.examples.run;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.testng.Assert;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.model.PostModel2;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by Larry on 10.08.2016.
 */
public class RESTTest {
    @Test
    public void testingRest() throws IOException{
        Response r = Request.Get("http://localhost:3000/posts").execute();
        HttpResponse httpResponse = r.returnResponse();

        int statusCode = httpResponse.getStatusLine().getStatusCode();
        String reasonPhrase = httpResponse.getStatusLine().getReasonPhrase();
        System.out.println("Response was: " + statusCode + " - " + reasonPhrase);

        InputStream bodyAsInputStream = httpResponse.getEntity().getContent();
        String responseBody = IOUtils.toString(bodyAsInputStream);
        System.out.println(responseBody);
    }
    @Test
    public void restPostTest() throws IOException{
        Response r = Request
                .Post("http://localhost:3000/posts")
                .bodyString("{\"title\": \"foo\", \"author\": \"bar\"}",
                        ContentType.APPLICATION_JSON)
                .execute();
    }
    @Test
    public void restDeleteTest() throws IOException{
        Response rrr = Request.Delete("http://localhost:3000/posts/2").execute();
    }

    @Test
    public void restObjectTest() throws IOException{

        Response r = Request.Post("http://localhost:3000/posts").
                bodyString("{\"id\": 7, \"title\": \"foo\", \"author\": \"bar\"}"
                        ,ContentType.APPLICATION_JSON).execute();
        HttpResponse httpResponse = r.returnResponse();

        InputStream bodyAsInputStream = httpResponse.getEntity().getContent();
//        System.out.println(IOUtils.toString(bodyAsInputStream));
        ObjectMapper om = new ObjectMapper();
        PostModel2 pm = om.readValue(bodyAsInputStream, PostModel2.class);
        System.out.println(pm.getId());

        //ObjectMapper mapper = new ObjectMapper();
        //String jsonInString = "{\"id\": 10, \"title\": \"foo\", \"author\": \"bar\"}";
        //PostModel obj = mapper.readValue(new URL("http://localhost:3000/posts"), PostModel.class);
        //PostModel obj = mapper.readValue(jsonInString, PostModel.class);

    }
}

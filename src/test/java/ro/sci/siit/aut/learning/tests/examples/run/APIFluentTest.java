package ro.sci.siit.aut.learning.tests.examples.run;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Larry on 08.08.2016.
 */
public class APIFluentTest {
    @Test
    public void testingAPIFluent() throws IOException {
        Response r = Request.Get("http://shop-tausandbox.rhcloud.com").execute();
        HttpResponse httpResponse = r.returnResponse();

        int statusCode = httpResponse.getStatusLine().getStatusCode();
        String reasonPhrase = httpResponse.getStatusLine().getReasonPhrase();
        System.out.println("Response was: " +  statusCode + " - " + reasonPhrase);

        InputStream bodyAsInputStream = httpResponse.getEntity().getContent();
        String responseBody = IOUtils.toString(bodyAsInputStream);
        System.out.println(responseBody);
    }
}

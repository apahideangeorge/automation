package ro.sci.siit.aut.learning.tests.examples;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.tests.BaseTest;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created by Larry on 29.06.2016.
 */
public class CookiesExample extends BaseTest {

    @BeforeMethod
    public void startBrowser() {
        driver.get("http://google.com");
    }

    @Test
    public void cookiesTest() {
        Set<Cookie> setOfCookies = driver.manage().getCookies();
        System.out.println("Found " + setOfCookies.size() + " cookies");
        for (Cookie c : setOfCookies) {
            System.out.println("Cookie name: " + c.getName());
            System.out.println("Cookie value " + c.getValue());
            System.out.println("Cookie class " + c.getClass());
            System.out.println("Cookie domain" + c.getDomain());
            System.out.println("Cookie path " + c.getPath());
            System.out.println("Cookie expiry " + c.getExpiry());
        }
    }

    @Test
    public void specificCookie(){
        Cookie cookie = driver.manage().getCookieNamed("CONSEN");
        if(cookie == null){
            System.out.println("The cookie does not exist, here is a list of existing cookies");
            Set<Cookie> setOfCookies = driver.manage().getCookies();
            for (Cookie c : setOfCookies){
                System.out.println(c.getName());
            }
        }else{
            System.out.println("Cookie name " + cookie.getName());
            System.out.println("Cookie value " + cookie.getValue());
        }
    }
    @Test
    public void addCookie(){
        driver.get("http://google.com");
        Cookie cookie = new Cookie("myCookie", "My cookie value");
        driver.manage().addCookie(cookie);

        Cookie c = driver.manage().getCookieNamed("myCookie");

        Assert.assertNotNull(cookie, "smth");

        if(c == null){
            System.out.println("The cookie does not exist");
            }
        }
    @Test
    public void cookieCompare()  {
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/cookie.html");
        WebElement cookieButton = driver.findElement(By.id("set-cookie"));
        cookieButton.click();
        WebElement cookieText = driver.findElement(By.id("cookie-value"));
        Cookie c = driver.manage().getCookieNamed("gibberish");
        Assert.assertEquals(cookieText.getText(), c.getValue());
    }

    @Test
    public void takeScreenshot()throws IOException{
        driver.get("http://google.com");
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File ("D:/Auto/screenshot.png");
        FileUtils.copyFile(screenshotFile, finalFile);
    }
    @Test
    public void noCookie() {
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/cookie.html");
        WebElement cookieButton = driver.findElement(By.id("set-cookie"));
        cookieButton.click();

        WebElement deleteCookieButton = driver.findElement(By.id("delete-cookie"));
        deleteCookieButton.click();

        WebElement cookieText = driver.findElement(By.id("cookie-value"));
        Cookie c = driver.manage().getCookieNamed("gibberish");
        Assert.assertNull(c);
        Assert.assertEquals(cookieText.getText(), "");
    }
}

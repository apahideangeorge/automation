package ro.sci.siit.aut.learning.tests.listener;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import java.lang.reflect.Method;

/**
 * Created by Larry on 11.07.2016.
 */
public class TestListener extends TestListenerAdapter{
    public void onTestStart(ITestResult result){

        String testMethodName = result.getMethod().getMethodName();
        String testDescription = result.getMethod().getDescription();
        System.out.println();
        System.out.println("Start Test " + testMethodName);

        if (testDescription != null){
            if (!testDescription.isEmpty()){
                System.out.println("Description " + testDescription);
            }
        }

    }
    public void onTestSuccess(ITestResult result){
        System.out.println("Successfull Test");
    }

    public void onTestFailure(ITestResult result){
        System.out.println(":(");
    }

    public void onTestSkipped(ITestResult result){
        System.out.println("This test has been skipped");
    }
}

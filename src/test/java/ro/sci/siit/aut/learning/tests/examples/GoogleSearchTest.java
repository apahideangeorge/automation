package ro.sci.siit.aut.learning.tests.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.tests.BaseTest;

/**
 * Created by Larry on 01.06.2016.
 */
public class GoogleSearchTest extends BaseTest {

    //TODO do something
    //FIXME this is a bug

    @BeforeMethod
    public void startBrowser() {
        driver.get("http://google.com");
    }

    @Test(
            enabled = false,
            groups = "search"
    )
    public void firstSearch() throws InterruptedException {
        searchGoogle("koala");
        Thread.sleep(2000);
    }

    @Test(
            enabled = false,
            groups = {"search", "smoke"}
    )
    public void secondSearch() {
        searchGoogle("grizzly");
    }

    @DataProvider(name = "BearsDataProvider")
    public Object[][] dataProviderForSearchTest() {
        return new Object[][]{
                {"Koala", "Animal"},
                {"Grizzly", "Animal"}
        };
    }

    @Test(dataProvider = "BearsDataProvider")
    public void bearsTest(String p1, String p2) {
        searchGoogle(p1);
        WebElement animalNameWIKI = driver.findElement(By.cssSelector("div[class='kno-ecr-pt kno-fb-ctx']"));
        System.out.println(animalNameWIKI.getText());
        Assert.assertEquals(animalNameWIKI.getText(), p1);
        WebElement animalTypeWIKI = driver.findElement(By.cssSelector("div[class='_gdf']"));
        System.out.println(animalTypeWIKI.getText());
        Assert.assertEquals(animalTypeWIKI.getText(), p2);
        System.out.println(p1 + " is an " + p2);
    }

    public void searchGoogle(String word) {
        WebElement searchBox = driver.findElement(By.id("lst-ib"));
        searchBox.sendKeys(word);
        WebElement searchButton = driver.findElement(By.name("btnG"));
        searchButton.click();

        WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.id("resultStats")));
        //System.out.println(driver.findElement(By.id("resultStats")).getText());

        //List<WebElement> resultTitleElements = driver.findElements(By.tagName("h3"));
        //System.out.println(resultTitleElements.size());

        //for (WebElement we : resultTitleElements)
        //{
        //    System.out.println(we.getText());
        //    WebElement resultUrl = we.findElement(By.xpath("//a"));
        //   String url = resultUrl.getAttribute("href");
        //  System.out.println(url);
        //}


        //Integer noOfRes = resultTitleElements.size();
        //Assert.assertFalse(noOfRes.equals(""));
        //Assert.assertEquals(resultTitleElements.size(), 10);
    }

}
package ro.sci.siit.aut.learning.model;

/**
 * Created by Larry on 06.06.2016.
 */
public class LoginModel {
    private String username;
    private String password;
    private String usernameError;
    private String passwordError;
    private String generalError;

    public LoginModel(String username, String password, String usernameError, String passwordError, String generalError) {
        this.username = username;
        this.password = password;
        this.usernameError = usernameError;
        this.passwordError = passwordError;
        this.generalError = generalError;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsernameError() {
        return usernameError;
    }

    public void setUsernameError(String usernameError) {
        this.usernameError = usernameError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public String getGeneralError() {
        return generalError;
    }

    public void setGeneralError(String generalError) {
        this.generalError = generalError;
    }
}



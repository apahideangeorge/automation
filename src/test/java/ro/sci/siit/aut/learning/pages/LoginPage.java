package ro.sci.siit.aut.learning.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Larry on 15.06.2016.
 */
public class LoginPage {
    @FindBy (how = How.ID, using = "input-login-username")
    private WebElement userNameField;

    @FindBy (how = How.XPATH, using = "//input[@id='input-login-username']/following-sibling::div")
    private WebElement userErrorElement;

    @FindBy (how = How.ID, using = "input-login-password")
    private WebElement passwordField;

    @FindBy (how = How.XPATH, using = "//input[@id='input-login-password']/following-sibling::div")
    private WebElement passwordErrorElement;

    @FindBy (how = How.ID, using = "login-submit")
    private WebElement loginButton;

    @FindBy (how = How.ID, using = "login-error")
    private WebElement generalErrorElement;

    public String userError() {return userErrorElement.getText();}
    public String passwordError(){return passwordErrorElement.getText();}
    public String generalError(){return generalErrorElement.getText();}

    public void login (String user, String password)
    {
        userNameField.clear();
        userNameField.sendKeys(user);
        passwordField.clear();
        passwordField.sendKeys(password);
        loginButton.click();
    }
}
package ro.sci.siit.aut.learning.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.sci.siit.aut.learning.model.RegistrationModel;
import ro.sci.siit.aut.learning.pages.RegistrationPage;

/**
 * Created by Larry on 25.06.2016.
 */
public class RegistrationTest {
    WebDriver driver;

    @BeforeMethod
    public void startFirefox() {
        driver = new FirefoxDriver();
        driver.get("http://a1-tausandbox.rhcloud.com/web-mocks/mocks/registration.html");
    }

    @AfterMethod
    public void stopFirefox() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }

    @DataProvider(name = "RegistrationDataProvider")
    public Object[][] dataProviderForRegistrarionTest() {
        Object[][] dpObject = new Object[4][2];

        RegistrationModel model = new RegistrationModel("", "", "", "", "", "", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please fill out this field.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Please fill out this field.",
                "Please fill out this field.");
        dpObject[0][0] = "failed";
        dpObject[0][1] = model;

        model = new RegistrationModel("", "", "notAnEmail", "", "short", "short", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please enter an email address.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Not long enough",
                "");

        dpObject[1][0] = "failed";
        dpObject[1][1] = model;

        model = new RegistrationModel("", "", "notAnEmail", "", "password", "short", "29", "12", "1986",
                "Invalid input. Please enter between 2 and 35 letters",
                "Invalid input. Please enter between 2 and 35 letters",
                "Please enter an email address.",
                "Invalid input. Please enter between 4 and 35 letters or numbers",
                "Minimum of 8 characters",
                "The passwords don't match");

        dpObject[2][0] = "failed";
        dpObject[2][1] = model;

        model = new RegistrationModel("first", "last", "a@a.com", "user", "password", "password", "29", "12", "1986",
                "",
                "",
                "",
                "",
                "",
                "");

        dpObject[3][0] = "succes";
        dpObject[3][1] = model;

        return dpObject;
    }

    @Test(dataProvider = "RegistrationDataProvider")
    public void registrationTest (String expected, RegistrationModel model){
        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        registrationPage.register(model.getFirstName(), model.getLastName(), model.getEmail(),model.getUsername(),
                model.getPassword(), model.getConfirmPassword(),
                model.getDayOfBirth(), model.getMonthOfBirth(), model.getYearOfBirth());
        if (expected == "failed"){
            Assert.assertEquals(registrationPage.firstNameError(), model.getFirstNameError(), "Checking first name error");
            Assert.assertEquals(registrationPage.lastNameError(), model.getLastNameError());
            Assert.assertEquals(registrationPage.emailError(), model.getEmailError());
            Assert.assertEquals(registrationPage.usernameError(), model.getUsernameError());
            Assert.assertEquals(registrationPage.passwordError(), model.getPasswordError());
            Assert.assertEquals(registrationPage.passwordError2(), model.getConfirmPasswordError());
        }else{
            WebElement registrationButton = driver.findElement(By.id("register-submit"));
            registrationButton.click();
            WebElement logoutButton = driver.findElement(By.id("logout-submit"));
            String logoutButtonText = logoutButton.getText();
            Assert.assertEquals(logoutButtonText, "Logout");
        }
    }
}